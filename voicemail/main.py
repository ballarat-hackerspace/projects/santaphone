"""Call Santa!

Pick up the receiver, dial in Santa's number (72672) and leave a message for santa

Actions happen when a correct sequence is dialed, see "actions" towards the bottom of
this file.

State is recorded in the state dictionary.




Current input is cleared when...
- The handset is put down, in which case it is silently cleared
- A # is dialed, in which case the sequence is shown and then cleared
- A sequence is entered, in which case the action happens and then input is cleared

"""

import time
import serial
import logging
import subprocess
from datetime import datetime


HANDSET_DOWN = False
HANDSET_UP = True
MIN_SECONDS_FOR_RECEIPT = 15  # Must listen to this many seconds before a receipt will print


# Allow cycling through messages from Santa
santa_messages = [
    "data/1121_ChristmasWishList.ogg",
    "data/1128_ChristmasWishList2.ogg"
]

state = {
    'handset': HANDSET_DOWN,  # Is the handset up?
    'dialed': "",  # What numbers have been dialed?
    'player': None,
    'player_started': None,  # Note: only interested in the "Santa message" player
    'santa_message_index': 0, # Index of current message
}


def hahaha(log):
    print("The count says...")
    print("VONE, TWO, THREE")
    print("HA HA HA")
    log.info("HA HA HA")


def play_test(log):
    log.info("Performing test routine")
    log.info("Printing test receipt")
    subprocess.Popen(['lp', "data/test.pdf"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    log.info("Playing test sound")
    subprocess.Popen(['mplayer', "data/busy.ogg"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    log.info("Tests sent, I cannot confirm or deny that worked")


def play_audio(log):
    state['player_started'] = datetime.now()

    # Cycle through messages from Santa
    state['santa_message_index'] = (state['santa_message_index'] + 1) % len(santa_messages)
    santa_message = santa_messages[state['santa_message_index']]
    log.info("PLAY,Playing message from file {}".format(santa_message))

    state['player'] = subprocess.Popen(['mplayer', "data/ring.ogg", "data/ring.ogg", santa_message, "data/sleighbells.ogg"],
                              stdin=subprocess.PIPE,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)


def handset_up():
    state['handset'] = HANDSET_UP


def handset_down():
    state['handset'] = HANDSET_DOWN


def setup_logging(log_name, filename):
    logger = logging.getLogger(log_name)
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler(filename)
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s,%(name)s,%(levelname)s,%(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger


def main(debug=False):
    log_filename = "/var/log/statistics.log"
    if debug:
        # when debugging, we put log locally, as we probably can't put logs in /var/logs
        log_filename = "statistics.log"
    events = setup_logging("STATS", log_filename)
    events.info("PROGRAM,STARTED")
    with serial.Serial("/dev/ttyUSB0") as ser:
        print("Connected")
        events.info("CONNECTED,SERIAL")
        while True:
            time.sleep(0.1)
            for c in ser.read():
                #c = chr(c).strip()
                if not isinstance(c, str):
                    # Likely on Python 3
                    c = chr(c)
                c = c.strip()
                if c:
                    print(c)
                    # Log "everything" at DEBUG level
                    events.debug("INPUT,{}".format(c))
                if c == "U":
                    state['handset'] = HANDSET_UP
                    events.info("HANDSET_UP")
                    print("Running action for {}".format("72682"))
                    events.info("RUN,{}".format("72682"))
                    actions["72682"](events)
                    state['dialed'] = ""
                elif c == "D":
                    state['handset'] = HANDSET_DOWN
                    events.info("HANDSET_DOWN")
                    state['dialed'] = ""
                    if state['player']:
                        try:
                            # state['player'].stdin.write(b"q")
                            state['player'].kill()
                        except Exception as e:
                            print("mplayer already quit?", str(e))
                            events.warning("MPLAYER,ERROR,{}".format(e))
                        finally:
                            state['player'] = None

                        if state['player_started']:
                            seconds_played = (datetime.now() - state['player_started']).total_seconds()
                            if seconds_played > MIN_SECONDS_FOR_RECEIPT:
                                events.info("SANTA,Printing receipt from Santa")
                                events.info("SANTA,TIME,{}".format(seconds_played))
                                subprocess.Popen(['lp', "data/message.pdf"],
                                    stdin=subprocess.PIPE,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
                            else:
                                events.info("SANTA,Not printing receipt from Santa - only listened for {} seconds".format(seconds_played))
                        print("Done")
                elif c in '0123456789':
                    state['dialed'] += c
                elif c == "#":
                    # Print state, then clear it
                    print(state['dialed'])
                    state['dialed'] = ""
                elif c == "*":
                    print("You're a star!")

                if state['player'] is None and state['handset'] and state['dialed'] in actions:
                    print("Running action for {}".format(state['dialed']))
                    events.info("RUN,{}".format(state['dialed']))
                    actions[state['dialed']](events)
                    state['dialed'] = ""
        events.warning("SERIAL,DISCONNECTING")
    events.warning("PROGRAM,CLOSED")
    print("Exited")



actions = {
    "123": hahaha,
    "72682": play_audio,  # SANTA
    "1337": play_test,  # test audio and printer
}


if __name__ == "__main__":
    import sys
    debug_mode = False
    if len(sys.argv) > 1:
        debug_mode = (sys.argv[1] == 'debug')
    main(debug=debug_mode)
